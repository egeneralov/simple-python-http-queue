from os import environ
import datetime
import logging
import sys
import json_logging
import json
import flask


QUEUE = []


app = flask.Flask(__name__)
# suppress flask messages
environ["WERKZEUG_RUN_MAIN"] = "true"

json_logging.ENABLE_JSON_LOGGING = True
json_logging.init(
  framework_name='flask'
)
json_logging.init_request_instrument(app)
# init the logger as usual
logger = logging.getLogger("pre-init")
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))



@app.route("/", methods = ["GET"])
def GET():
  try:
    r = QUEUE.pop()
  except IndexError:
    return flask.jsonify([])
  try:
    if not r["download_links"]:
      return GET()
  except KeyError:
    return flask.jsonify([])
  return flask.jsonify([r])



@app.route("/", methods = ["POST"])
def POST():
  data = flask.request.get_data()
  data = json.loads(data)
  QUEUE.append(data)
  return flask.jsonify([ data ])



if __name__ == "__main__":
  port = environ.get("PORT", "8080")
  host = environ.get("BIND", '0.0.0.0')
  try:
    logger.info(
      f"Starting application on {host}:{port}"
    )
    app.run(
      port = port,
      host = host,
      debug = False
    )
  except OSError:
    logger.critical(
      f"Can't bind to {host}:{port}"
    )
