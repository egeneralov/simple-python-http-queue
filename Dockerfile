FROM python:3

WORKDIR /app/

RUN pip install pyinstaller

ADD requirements.txt /app/

RUN pip install --no-cache-dir -r /app/requirements.txt

ADD . .

RUN pyinstaller --console --onefile --name q app.py


FROM debian:9

COPY --from=0 /app/dist/q /

ENV BIND='0.0.0.0' PORT='8080'
EXPOSE ${PORT}

ENTRYPOINT ["/q"]
