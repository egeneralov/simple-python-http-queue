q:
	pyinstaller --distpath . --console --onefile --name q app.py

clean:
	find . -name __pycache__ -type d | xargs rm -rf q.spec build
	rm -rf q.spec build q
